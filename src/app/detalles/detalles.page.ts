import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.page.html',
  styleUrls: ['./detalles.page.scss'],
})
export class DetallesPage implements OnInit {
  motos;
  
  ngOnInit() {
    // this.getJson()
  }
  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.motos = this.router.getCurrentNavigation().extras.state.parametros;
        console.log(this.motos);
      }
    });
  }
  
  deleteMoto(idMoto) {
    const url = "http://motos.puigverd.org/moto/" + idMoto;
    fetch(url, {
      "method": "DELETE"
    })
    .then(response => {
        this.router.navigateByUrl('/home');
      });
  }
}
