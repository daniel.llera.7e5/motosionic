import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(private router: Router,private menu: MenuController) {}
  motos;
  ngOnInit() {
    this.getJson()
  }
  ionViewWillEnter(){
    this.getJson()

  }
  async getJson(){
    const respuesta = await
  fetch("http://motos.puigverd.org/motos");
    var motos = await respuesta.json();
    this.motos = motos
    console.log(motos);
    this.menu.enable(false, 'first');

}

async getDucati(){
  const respuesta = await
fetch("http://motos.puigverd.org/motos?marca=Ducati");
  var motos = await respuesta.json();
  this.motos = motos
  console.log(motos);
  this.menu.enable(false, 'first');
}

async getYamaha(){
  const respuesta = await
fetch("http://motos.puigverd.org/motos?marca=Yamaha");
  var motos = await respuesta.json();
  this.motos = motos
  console.log(motos);
  this.menu.enable(false, 'first');
}

async getHonda(){
  const respuesta = await
fetch("http://motos.puigverd.org/motos?marca=Honda");
  var motos = await respuesta.json();
  this.motos = motos
  console.log(motos);
  this.menu.enable(false, 'first');
}

  addMore(){
    this.router.navigate(['/add']);

  }

  detalles(id){
    var moto;
    this.motos.forEach(element => {
        if (element.id == id) {
          moto = element
        }
    });
    let navigationExtras: NavigationExtras = {
      state: {
       parametros: moto
      }
    };
    this.router.navigate(['/detalles'], navigationExtras);
  } 

  openMenu() {
    console.log("open")
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }
}
